﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.Write("Enter size: ");
int.TryParse(Console.ReadLine(), out int size);
var array = new int[size];
int index = 0;
foreach (var item in array)
{
    bool check;
    do
    {
        Console.Write($"Phần tử thứ {index + 1}: ");
        check = int.TryParse(Console.ReadLine(), out array[index]);
    } while (!check);
    index++;
}
Console.WriteLine($"Số lớn nhất trong mảng là:{array.Max(x => x)}");
