﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.Write("Nhập kích thước của mảng: ");
int.TryParse(Console.ReadLine(), out int size);
var array = new int[size];
int index = 0;
foreach (var item in array)
{
    bool check;
    do
    {
        Console.Write($"Phần tử thứ {index + 1}: ");
        check = int.TryParse(Console.ReadLine(), out array[index]);
    } while (!check);
    index++;
}
Console.Write("Mảng bạn vừa nhập là: ");
foreach (var item in array)
{
    Console.Write(item + " ");
}

int firstNum = array[0];
for (int i = 0; i < array.Length - 1; i++)
{
    array[i] = array[i + 1];
}
array[array.Length - 1] = firstNum;
Console.Write("\nMảng sau khi dịch sang trái:");
foreach (var item in array)
{
    Console.Write(item + " ");
}