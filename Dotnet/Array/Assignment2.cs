﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Console.Write("Nhập kích thước của mảng: ");
int.TryParse(Console.ReadLine(), out int size);
var array = new int[size];
int index = 0;
foreach (var item in array)
{
    bool check;
    do
    {
        Console.Write($"Phần tử thứ {index + 1}: ");
        check = int.TryParse(Console.ReadLine(), out array[index]);
    } while (!check);
    index++;
}
for (int i = 0; i < array.Length / 2; i++)
{
    int temp = array[i];
    array[i] = array[array.Length - 1 - i];
    array[array.Length - 1 - i] = temp;
}
Console.WriteLine("Mảng sau khi hoán đổi:");

foreach (int num in array)
{
    Console.Write(num + " ");
}