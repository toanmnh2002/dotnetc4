﻿int[] array = Array.Empty<int>();

void PrintArray(int[] array)
{
    if (array.Length <= 0)
    {
        Console.WriteLine("Mảng đang không có phần tử");
    }
    else
    {
        Console.WriteLine("Mảng hiện tại là: ");
        foreach (var item in array)
        {
            Console.Write(item + " ");
        }
    }
}

void AddNum()
{
    int number;
    do
    {
        Console.Write("Nhập số để thêm vào mảng: ");
    } while (!int.TryParse(Console.ReadLine(), out number));
    Array.Resize(ref array, array.Length + 1);
    array[array.Length - 1] = number;
}
void Menu()
{
    bool back = false;
    do
    {
        int option;
        Console.WriteLine("\n1.Xem danh sách số đã được lưu trong mảng");
        Console.WriteLine("2.Thêm một số mới vào danh sách");
        Console.WriteLine("3.Thoát chương trình");
        do
        {
            Console.Write("Nhập lựa chọn của bạn: ");
        } while (!int.TryParse(Console.ReadLine(), out option));

        switch (option)
        {
            case 1:
                PrintArray(array);
                break;
            case 2:
                AddNum();
                break;
            case 3:
                back = true;
                break;
            default:
                Console.WriteLine("Lựa chọn không hợp lệ!!!");
                break;
        }
    } while (!back);
}
Menu();
