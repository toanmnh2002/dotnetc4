﻿#region HomeGift1
/*Console.OutputEncoding = System.Text.Encoding.Default;
Console.Write("Enter Year of Birth: ");
int.TryParse(Console.ReadLine(),out int yearOfBirth);
Console.Write("Enter height: ");
int.TryParse(Console.ReadLine(), out int height);
Console.Write("Enter weight: ");
int.TryParse(Console.ReadLine(), out int weight);
Console.Write("Enter point: ");
double.TryParse(Console.ReadLine(), out double point);
Console.Write("Enter your marriage status: ");
string status = Console.ReadLine();
Console.WriteLine("\nThông tin đã nhập:\n" +
    $"Năm sinh:{yearOfBirth}\n" +
    $"Chiều cao:{height}\n" +
    $"Cân nặng:{weight}\n" +
    $"Điểm trung bình:{point}\n" +
    $"Trạng thái hôn nhân:{status}\n");*/
#endregion

#region HomeGift2
/*Console.OutputEncoding = System.Text.Encoding.Default;
Console.Write("Nhập giá trị thứ nhất: ");
int.TryParse(Console.ReadLine(),out int firstNum);
Console.Write("Nhập giá trị thứ hai: ");
int.TryParse(Console.ReadLine(), out int secondNum);

int temp = firstNum;
firstNum = secondNum;
secondNum = temp;

Console.WriteLine($"\nGiá trị trước khi đổi chỗ: " +
    $"\nGiá trị thứ nhất: {firstNum}" +
    $"\nGiá trị thứ hai: {secondNum}");
Console.Write("\nGiá trị sau khi đổi chỗ: : ");
Console.Write("\nGiá trị thứ nhất : " + firstNum);
Console.Write("\nGiá trị thứ hai : " + secondNum);*/
#endregion

#region HomeGift3
/*Console.OutputEncoding = System.Text.Encoding.Default;
Console.Write("Nhập chiều dài của hình chữ nhật:");
double.TryParse(Console.ReadLine(),out double chieuDai);
Console.Write("Nhập chiều rộng của hình chữ nhật:");
double.TryParse(Console.ReadLine(), out double chieuRong);
Console.Write($"Chu vi của hình chữ nhật là:{(chieuDai + chieuRong) * 2}");*/
#endregion


#region HomeGift4
Console.OutputEncoding = System.Text.Encoding.Default;
Console.Write("Nhập số mảnh quặng đã thu thập:");
int.TryParse(Console.ReadLine(), out int num);
int coin = 0;
coin += Math.Min(num, 10) * 10;
num -= Math.Min(num, 10);
coin += Math.Min(num, 5) * 5;
num -= Math.Min(num, 5);
coin += Math.Min(num, 3) * 2;
num -= Math.Min(num, 3);
coin += num;
Console.WriteLine("Số lượng đồng xu vàng mà bạn nhận được là: " + coin);
#endregion
